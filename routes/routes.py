from fastapi import APIRouter, Depends
from schemas.schema import UserCreate, AddressCreate
from config.database import SessionLocal, Base, engine
from models.models import User, Address
import sqlite3


app = APIRouter()

Base.metadata.create_all(bind=engine)


def get_db_connection():
    conn = sqlite3.connect('database.db', check_same_thread=False)
    return conn
 
 
@app.post("/users/")
def create_user(user: UserCreate, address: AddressCreate, db: sqlite3.Connection = Depends(get_db_connection)):
    cur = db.cursor()
    try:
        print(f"User Data: {user}")
        print(f"Address Data: {address}")

        cur.execute("INSERT INTO user (first_name, last_name, email, password) VALUES (?, ?, ?, ?)", 
                       (user.first_name, user.last_name, user.email, user.password))
        
        id = cur.lastrowid
        
        cur.execute("INSERT INTO address (address_1, address_2, city, state, zip, country) VALUES (?, ?, ?, ?, ?, ?)", 
                       (address.address_1, address.address_2, address.city, address.state, 
                        address.zip, address.country))
        db.commit()
        return 'User Created Successfully!!!'
    except Exception as e:
        db.rollback()
        print(f"Error: {str(e)}") 
              

@app.get("/users/{country}")
async def get_country(country: str, db: sqlite3.Connection = Depends(get_db_connection)):
    cur = db.cursor()
    try:
        cur.execute('''SELECT u.id, u.first_name, u.last_name, u.email, a.address_1, a.address_2, a.city, a.state, a.zip, a.country
                          FROM user AS u
                          JOIN address AS a 
                            ON u.id = a.id WHERE a.country = ?''',
                       (country,))
        #users = cur.fetchall()
        rows = cur.fetchall()
        
        column_names = [desc[0] for desc in cur.description]
        
        users = []
        for row in rows:
            user_dict = dict(zip(column_names, row))
            users.append(user_dict)
        
        return {"users": users}
    except Exception as e:
        print(f"Error: {str(e)}")