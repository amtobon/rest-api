import requests
from data import url, new_user, country


# Create a new user
response = requests.post(f"{url}/users/", json=new_user)
if response.status_code == 200:
    created_user = response.json()
    print("Created User:")
    print(created_user)
else:
    print("Failed to create user")

# Retrives the info of users by country
response = requests.get(f"{url}/users/{country}/")
if response.status_code == 200:
    users_country = response.json()
    print(f"Users in {country}:")
    print(users_country)
else:
    print(f"Failed to retrieve users in {country}")