from pydantic import BaseModel


class UserCreate(BaseModel):
    first_name: str
    last_name: str 
    email: str
    password:str 

class AddressCreate(BaseModel):
    address_1: str
    address_2: str
    city: str
    state: str
    zip: str
    country: str