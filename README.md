# REST services for user administration

This project consists in the use of FastAPI to create RESTful services for user administration. It includes two enpoints, one for create users and another for retrieving user information. Inter-service communication can be achieved via RESTful API calls.

## Table of Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Structure](#structure)
- [Usage](#usage)
  - [Running the Services](#running-the-services)
  - [Creating Users](#creating-users)
  - [Retrieving Users by Country](#retrieving-users-by-country)
  - [Inter-Service Communication](#inter-service-communication)

## Introduction

This project consists of two endpoints:

1. **User Management Service**: This endpoint allows you to create users with input parameters such as first name, last name, email, password, address_1, address_2, city, state, zip and country.

2. **User Retrieval Service**: This endpoint retrieves user information such as id, first_name, last_name, email, address_1, address_2, city, state and zip by country.

For inter-service communication between these services is used RESTful API calls.


## Requirements

The main libraries used for this project are:

- FastAPI: A modern web framework for building APIs with Python.

- SQLite: A lightweight, file-based relational database used for d- ata storage.

- SQLAlchemy: A powerful SQL toolkit and Object-Relational Mapping (ORM) library for Python.

### Prerequisites

This project was developed using Python 3.9. While it may work with other Python versions, it's recommended to use Python 3.9 for optimal compatibility.

### Installation

To ensure correct operation, install the file requirements.txt as follows:
`pip install -r requirements.txt`


## Structure

The folder structure is organized as follows:

.
├── config
│   └── database.py
├── data.py
├── database.db
├── main.py
├── models
│   └── models.py
├── request.py
├── requirements.txt
├── routes
│   └── routes.py
├── schemas
│   └── schema.py

-   `config/`: Contains the configuration of database. 
-  `schemas/`: Contains the definition of the schemas to define the structure of the data using Pydantic..
-    `models/`: Includes database models for user and address tables. 
-  `routes/`: Contains route definitions and API endpoints for create a user and retrieve users by country .
-   `main.py`: Defines the FastAPI application. 
-   `requirements.txt`: Lists the project's dependencies. 
-   `request.py`:  script for making RESTful API requests.
-   `data.py`: script with the data to make a request, using the faker library to add random users.


## Usage  

### Running the Services  

1. Start the FastAPI application by running the following command: 
	`python main.py`
	or
	`uvicorn main:app --reload` 
    if you want to enable automatic code reloading during development.

The FastAPI application runs at `http://localhost:8000`. You can access it via a web browser.
If you want to interact with the API endpoints and see the documentation you can access it through the web using `http://localhost:8000/docs`.

### Creating Users

To create users, make a POST request to the `/users/` endpoint with the required user and address information. 

### Retrieving Users by Country

To retrieve users by country, make a GET request to the `/users/{country}` endpoint, where `{country}` is the desired country name.

### Inter-Service Communication

You can use the script `request.py `for inter-service communication. This script is designed to make RESTful API requests to the user administration service. Additionally, we provide another script `data.py` that facilitates the creation of new user data using the Faker library to generate random users. This script also includes a variable where you can specify the country you want to query in the API. To use these scripts effectively, ensure that the FastAPI application is running in the background before executing the `request.py` script.