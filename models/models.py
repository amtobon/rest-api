from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, declarative_base
from config.database import Base, engine

  
class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True) 
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    addresses = relationship("Address", back_populates="user")

class Address(Base):
    __tablename__ = "address"
    id = Column(Integer, primary_key=True, index=True)
    address_1 = Column(String)
    address_2 = Column(String)
    city = Column(String)
    state = Column(String)
    zip = Column(String)
    country = Column(String)
    #user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship("User", back_populates="address")
    